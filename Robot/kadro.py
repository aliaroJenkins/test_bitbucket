"""
Kadro module is used to interact with the K32 system
"""

from __future__ import absolute_import
import socket

PING_MSG = u"Ping\n"
MPB_INIT_MSG = u"MPB_Init({})\n"
MPB_GET_CHANNEL_MSG = u"MPB_GetChannel({},{})\n"
MPB_SET_CHANNEL_MSG = u"MPB_SetChannel({},{},{},{})\n"
MPB_SET_THRESHOLD_MSG = u"MPB_SetThreshold({},{},{})\n"

MCB_GET_LED_STATE_MSG = u"MCB_GetLedState({})\n"
MCB_SET_LED_STATE_MSG = u"MCB_SetLedState({},{})\n"
MCB_LOAD_CALIBRATION = u"MCB_LoadCalibration({})\n"
MCB_GET_BUTTON_STATE_MSG = u"MCB_GetButtonState({})\n"

PCB_GET_VOLTAGE_MSG = u"PCB_GetVoltage({})\n"
PCB_GET_CURRENT_MSG = u"PCB_GetCurrent({})\n"
PCB_SET_POWER_STATE_MSG = u"PCB_SetPowerState({},{})\n"
PCB_GET_POWER_STATE_MSG = u"PCB_GetPowerState({})\n"

BSB_INIT_MSG = u"BSB_Init({})\n"
BSB_CONNECT_BUS_MSG = u"BSB_ConnectBus({},{},{})\n"

DEMOCAR_SETSPEED_MSG = u"democar_setspeed({})\n"
DEMOCAR_SETSERVO_MSG = u"democar_setservo({})\n"
DEMOCAR_ANALOGREAD_A2_MSG = u"democar_analogread_a2()\n"
DEMOCAR_ANALOGREAD_A3_MSG = u"democar_analogread_a3()\n"
DEMOCAR_GETCOUNTER_MSG = u"democar_getcounter()\n"
DEMOCAR_RESETCOUNTER_MSG  = u"democar_resetcounter()\n"
DEMOCAR_CLOSEDAQ_MSG = u"democar_closedaq()\n"
DEMOCAR_INITDAQ_MSG = u"democar_initdaq()\n"
DEMOCAR_GET_AI0_MSG = u"democar_get_ai0()\n"
DEMOCAR_GET_AI3_MSG = u"democar_get_ai3()\n"
DEMOCAR_GETSPEED_MSG = u"democar_getspeed()\n"

STATIONSTORAGESPACE_MSG = u"StationStorageSpace({})\n"



class KadroCommunicationError(Exception):
    """ Object representing exception handling of received messages from K32-system"""
    def __init__(self, message):
        self.message = message
        super(KadroCommunicationError, self).__init__()


class KadroCalibrationException(Exception):
    """ Object representing exception handling of calibration handling for K32-system"""
    def __init__(self, message):
        self.message = message
        super(KadroCalibrationException, self).__init__()


class KadroConnection(object):
    """ Object representing a (open or closed) connection to the K32 system over TCP """
    def __init__(self):
        """
        Method for instantiating the KadroConnection class
        """
        self.host_ip = u"127.0.0.1"
        self.host_port = 1025
        self.timeout = 10

    def set_host(self, host_ip, host_port):
        """
        Method for updating the object-memory with communication settings
        :param host_ip:
        :param host_port:
        :return:
        """
        self.host_ip = host_ip
        self.host_port = host_port

    def _send_and_receive(self, message):
        """
        Method for handling the communication to the K32-system
        :param message:
        :return:
        """
        message = message.encode(u'ascii')
        kadro_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        kadro_socket.settimeout(self.timeout)
        try:
            kadro_socket.connect((self.host_ip, self.host_port))
            kadro_socket.send(message)
            msg_length = int(kadro_socket.recv(4), 16)
            received = kadro_socket.recv(msg_length)
        except socket.timeout:
            raise KadroCommunicationError(
                u"Connection to host {}:{} timed out.".format(self.host_ip, self.host_port))

        msg = dict()
        if received[0:4] == "OK (":
            msg[u'status'] = True
            msg[u'data'] = received.strip("^OK \(").strip("\)\n$").decode(u'ascii')
        elif received[0:7] == "ERROR (":
            msg[u'status'] = False
            msg[u'data'] = received.strip("^ERROR \(").strip("\)\n$").decode(u'ascii')
        else:
            msg[u'status'] = False
            msg[u'data'] = (u"ParsingError : (" + received + u")")
        return msg

    def check_connection(self):
        """
        Method for checking the communication through a ping to the K32-system
        :return:
        """
        if not self.ping()[u'status']:
            msg = u"No OK message received while attempting to ping host {}:{}". \
                format(self.host_ip, self.host_port)
            raise KadroCommunicationError(msg)
        else:
            return True

    def ping(self):
        """
        Method for sending the 'PING()'-command to the K32-system
        :return:
        """
        msg = PING_MSG
        return self._send_and_receive(msg)

    def mpb_init(self, card=1):
        """
        Method for sending the 'MPB_Init()'-command to the K32-system
        :param card:
        :return:
        """
        msg = MPB_INIT_MSG.format(card)
        return self._send_and_receive(msg)

    def mpb_get_channel(self, card=1, channel=1):
        """
        Method for sending the 'MPB_GetChannel()'-command to the K32-system
        :param card:
        :param channel:
        :return:
        """
        msg = MPB_GET_CHANNEL_MSG.format(card, channel)
        return self._send_and_receive(msg)

    def mpb_set_channel(self, card=1, channel=1, relay=1, state=0):
        """
        Method for sending the 'MPB_SetChannel()'-command to the K32-system
        :param card:
        :param channel:
        :param relay:
        :param state:
        :return:
        """
        msg = MPB_SET_CHANNEL_MSG.format(card, channel, relay, state)
        return self._send_and_receive(msg)

    def mpb_set_threshold(self, card=1, channel=1, threshold=0):
        """
        Method for sending the 'MPB_SetThreshold()'-command to the K32-system
        :param card:
        :param channel:
        :param threshold:
        :return:
        """
        msg = MPB_SET_THRESHOLD_MSG.format(card, channel, threshold)
        return self._send_and_receive(msg)

    def mcb_get_led_state(self, led=1):
        """
        Method for sending the 'MCB_GetLedState()'-command to the K32-system
        :param led:
        :return:
        """
        msg = MCB_GET_LED_STATE_MSG.format(led)
        return self._send_and_receive(msg)

    def mcb_set_led_state(self, led=1, state=1):
        """
        Method for sending the 'MPB_SetLedState()'-command to the K32-system
        :param led:
        :param state:
        :return:
        """
        msg = MCB_SET_LED_STATE_MSG.format(led, state)
        return self._send_and_receive(msg)

    def mcb_load_calibration(self, calib_data=""):
        """
        Method for loading new calibration data to the K32-system
        :param calib_data:
        :return:
        """
        msg = MCB_LOAD_CALIBRATION.format(calib_data)
        return self._send_and_receive(msg)

    def mcb_get_button_state(self, button=1):
        """
        Method for sending the 'MCB_GetButtonState()'-command to the K32-system
        :param button:
        :return:
        """
        msg = MCB_GET_BUTTON_STATE_MSG.format(button)
        return self._send_and_receive(msg)

    def pcb_get_voltage(self, target=1):
        """
        Method for sending the 'PCB_GetVoltage()'-command to the K32-system
        :param target:
        :return:
        """
        msg = PCB_GET_VOLTAGE_MSG.format(target)
        return self._send_and_receive(msg)

    def pcb_get_current(self, target=1):
        """
        Method for sending the 'PCB_GetCurrent()'-command to the K32-system
        :param target:
        :return:
        """
        msg = PCB_GET_CURRENT_MSG.format(target)
        return self._send_and_receive(msg)

    def pcb_set_power_state(self, target=1, state=1):
        """
        Method for sending the 'PCB_SetPowerState()'-command to the K32-system
        :param target:
        :param state:
        :return:
        """
        msg = PCB_SET_POWER_STATE_MSG.format(target, state)
        return self._send_and_receive(msg)

    def pcb_get_power_state(self, target=1):
        """
        Method for sending the 'PCB_GetPowerState()'-command to the K32-system
        :param target:
        :return:
        """
        msg = PCB_GET_POWER_STATE_MSG.format(target)
        return self._send_and_receive(msg)

    def bsb_init(self, card=1):
        """
        Method for sending the 'BSB_Init()'-command to the K32-system
        :param card:
        :return:
        """
        msg = BSB_INIT_MSG.format(card)
        return self._send_and_receive(msg)

    def bsb_connect_bus(self, card=1, bus=1, state=0):
        """
        Method for sending the 'BSB_ConnectBus()'-command to the K32-system
        :param card:
        :param bus:
        :param state:
        :return:
        """
        msg = BSB_CONNECT_BUS_MSG.format(card, bus, state)
        return self._send_and_receive(msg)

    def democar_setspeed(self, speed):
        """
        Method for sending the 'democar_setspeed()'-command to the K32-system
        :param speed:-90 - 90
        :return:
        """
        msg = DEMOCAR_SETSPEED_MSG.format(speed)
        return self._send_and_receive(msg)

    def democar_setservo(self, position):
        """
        Method for sending the 'democar_setservo()'-command to the K32-system
        :param speed:-90 - 90
        :return:
        """
        msg = DEMOCAR_SETSERVO_MSG.format(position)
        return self._send_and_receive(msg)

    def democar_analogread_a2(self):
        """
        Method for reading the 'democar_analogread_a2()'-command to the K32-system
        :return:
        """
        msg = DEMOCAR_ANALOGREAD_A2_MSG
        return self._send_and_receive(msg)

    def democar_analogread_a3(self):
        """
        Method for reading the 'democar_analogread_a3()'-command to the K32-system
        :return:
        """
        msg = DEMOCAR_ANALOGREAD_A3_MSG
        return self._send_and_receive(msg)

    def democar_getcounter(self):
        """
        Method for reading the 'democar_getcounter()'-command to the K32-system
        :return:
        """
        msg = DEMOCAR_GETCOUNTER_MSG
        return self._send_and_receive(msg)

    def democar_resetcounter(self):
        """
        Method for sending the 'democar_getcounter()'-command to the K32-system
        :return:
        """
        msg = DEMOCAR_RESETCOUNTER_MSG
        return self._send_and_receive(msg)

    def democar_closedaq(self):
        """
        Method for sending the 'democar_closedaq()'-command to the K32-system
        :return:
        """
        msg = DEMOCAR_CLOSEDAQ_MSG
        return self._send_and_receive(msg)

    def democar_initdaq(self):
        """
        Method for sending the 'democar_initdaq()'-command to the K32-system
        :return:
        """
        msg = DEMOCAR_INITDAQ_MSG
        return self._send_and_receive(msg)

    def democar_get_ai0(self):
        """
        Method for reading the 'democar_get_ai0()'-command to the K32-system
        :return:
        """
        msg = DEMOCAR_GET_AI0_MSG
        return self._send_and_receive(msg)

    def democar_get_ai3(self):
        """
        Method for reading the 'democar_get_ai3()'-command to the K32-system
        :return:
        """
        msg = DEMOCAR_GET_AI3_MSG
        return self._send_and_receive(msg)

    def democar_getspeed(self):
        """
        Method for reading the 'democar_getspeed()'-command to the K32-system
        :return:
        """
        msg = DEMOCAR_GETSPEED_MSG
        return self._send_and_receive(msg)

    def StationStorageSpace(self):
        '''
        Method for reading the 'station_storage_space():'-command to the K32-system
        :return:
        '''
        msg = STATIONSTORAGESPACE_MSG
        return self._send_and_receive(msg)

    def STATIONTYPE(self):
        '''
        Method for reading the 'station_storage_space():'-command to the K32-system
        :return:
        '''
        msg = STATIONTYPE_MSG
        return self._send_and_receive(msg)





if __name__ == u"__main__":
    k = KadroConnection()
    k.set_host(u"192.168.1.158", 1025)
    print k.ping()['data']
