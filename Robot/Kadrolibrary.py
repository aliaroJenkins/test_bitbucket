from kadro import KadroConnection, KadroCalibrationException, KadroCommunicationError
from unicodedata import numeric
import time

SLEEP_TIME = 0

class Kadrolibrary(object):
    """Test library for testing Kadro

    """

    def __init__(self, host_ip=u"192.168.1.149", host_port=1025):
        self._k = KadroConnection()
        self._result = ''
        self._host_ip = host_ip
        self._host_port = host_port
        self._k.set_host(self._host_ip, self._host_port)



    def check_kadro_connection(self):
        """
        Checking the Kadro communication
        :return:
        """
        self._result = self._k.check_connection()
        return self._result


    def pcb_set_power_state(self, target, state):
        """

        """
        time.sleep(SLEEP_TIME)
        self._result = self._k.pcb_set_power_state(target, state)
        return self._result

    def pcb_get_voltage(self, target):
        """

        """
        time.sleep(SLEEP_TIME)
        s = self._k.pcb_get_voltage(target)['data']
        self._result = int(s)
        return self._result

    def pcb_get_current(self, target):
        """

        """
        time.sleep(SLEEP_TIME)
        s = self._k.pcb_get_current(target)['data']
        self._result = int(s)
        return self._result

    def mpb_init(self, card):
        """Method for sending the 'MPB_Init()'-command to the K32-system
        :param card:
        :return:
        """
        time.sleep(SLEEP_TIME)
        self._result = self._k.mpb_init(card)
        return self._result

    def mpb_set_channel(self, card, channel, relay, state):
        """
        Method for sending the 'MPB_SetChannel()'-command to the K32-system
        :param card:
        :param channel:
        :param relay:
        :param state:
        :return:

        """
        time.sleep(SLEEP_TIME)
        self._result = self._k.mpb_set_channel(card, channel, relay, state)
        return self._result

    def mpb_get_channel(self, card, channel):
        """

        """
        self._result = self._k.mpb_get_channel(card, channel)
        return self._result

    def mcb_set_led_state(self, led, state):
        """
        Method for sending the 'MPB_SetLedState()'-command to the K32-system
        :param led:
        :param state:
        :return:
        """
        self._result = self._k.mcb_set_led_state(led, state)
        return self._result

    def result_should_be(self, expected):
        """Verifies that the current result is ``expected``.

        """
        print '*DEBUG* Got arguments {} and {}'.format(self._result, expected)
        if self._result == expected:
            raise AssertionError('{} != {}'.format(self._result, expected))

    def numbers_should_be_equal(self, expected):
        """Verifies that the current result is ``expected``.

        """
        print '*DEBUG* Got arguments {} and {}'.format(self._result, expected)
        if float(self._result) != float(expected):
            raise AssertionError('Given numbers are unequal!')

    def result_should_be_greater(self, expected):
        """Verifies that the current result is ``expected``.

        """
        print '*DEBUG* Got arguments {} and {}'.format(self._result, expected)
        if float(self._result) < float(expected):
            raise AssertionError('Given number is not greater!')

    def result_should_be_less(self, expected):
        """Verifies that result is less then 

        """
        print '*DEBUG* Got arguments {} and {}'.format(self._result, expected)
        if float(self._result) > float(expected):
            raise AssertionError('Given numbers are unequal!')


    def result_should_be_min_max(self, minimum, maximum):
        """Verifies that the current result is ``expected``.

        """
        print '*DEBUG* Got arguments {} and {}'.format(self._result, minimum, maximum)
        if float(self._result) < float(minimum):
            raise AssertionError('Given result: {} is less than min allowed value: {}'.format(self._result, minimum))
        elif float(self._result) > float(maximum):
            raise AssertionError('Given result: {} is grater then max allowed value: {}'.format(self._result,maximum))
        

def to_float(s):
    try:
        r = float(s)
    except ValueError:
        r = numeric(s)
    return r